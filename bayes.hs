module Bayes (bayes, trainFromDirectories) where

import System.Directory (getDirectoryContents, doesFileExist)
import System.FilePath (combine)
import Control.Monad (liftM, filterM, mapM)
import Data.List (foldl')
import qualified Data.Map as M
import qualified Data.Set as S

data TrainingSet = TrainingSet {
    tsIns :: M.Map String Int,
    tsOuts :: M.Map String Int,
    tsNumInWords :: Int,
    tsNumOutWords :: Int,
    tsInFiles :: S.Set FilePath,
    tsOutFiles :: S.Set FilePath
} deriving Show

train uws ufs f t = do
                    { fileData <- readFile f
                    ; let withFileT = liftM (ufs f) t
                    ; liftM (uws $ words fileData) withFileT 
                    }

incWord Nothing = Just 1
incWord (Just x) = Just (x+1)

trainIn f t = train updateInWords updateInFiles f t
    where updateInFiles f t = t{tsInFiles=S.insert f (tsInFiles t)}
          updateInWords ws t = foldl' upInWs t ws
          upInWs t w = t{tsIns=M.alter incWord w (tsIns t)}

trainOut f t = train updateOutWords updateOutFiles f t
    where updateOutFiles f t = t{tsOutFiles=S.insert f (tsOutFiles t)}
          updateOutWords ws t = foldl' upOutWs t ws
          upOutWs t w = t{tsOuts=M.alter incWord w (tsOuts t)}

trainWith f d t = do 
                  { cts <- getDirectoryContents d 
                  ; let contents = map (combine d) cts
                  ; files <- filterM doesFileExist contents
                  ; foldr f t files
                  }

totalData ts = sumIns $ sumOuts ts
    where sumIns t = t{tsNumInWords=M.fold (+) 0 (tsIns t)}
          sumOuts t = t{tsNumOutWords=M.fold (+) 0 (tsOuts t)}

trainFromDirectories :: FilePath -> [FilePath] -> IO TrainingSet
trainFromDirectories ins outs = liftM totalData $ foldr trainOuts trainIns outs
    where trainOuts = trainWith trainOut 
          trainIns = trainWith trainIn ins $ return newTraining
          newTraining = TrainingSet{tsIns=M.empty,tsOuts=M.empty,
                                    tsNumInWords=0, tsNumOutWords=0,
                                    tsInFiles=S.empty,tsOutFiles=S.empty}

bayes :: TrainingSet -> String -> Float 
bayes t d = pRatio t d 
    where pRatio t d = (pIn t) * (foldl' (*) 1 (map (pWord t) $ words d) ) 
          pIn t = (iToF $ S.size (tsInFiles t)) / (iToF $ S.size (tsOutFiles t))
          pWord t w = (max 0.01 (wordIn t w)) / (max 0.01 (wordOut t w))
          wordIn t w = iToF (M.findWithDefault 0 w $ tsIns t) / iToF (S.size $ tsInFiles t)
          wordOut t w = iToF (M.findWithDefault 0 w $ tsOuts t) / iToF (S.size $ tsOutFiles t)
          iToF = fromInteger . toInteger

