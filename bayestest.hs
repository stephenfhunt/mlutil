module Main where

import System.Environment (getArgs)
import Bayes

main = do 
       { args <- getArgs
       ; trainingData <- trainFromDirectories "ins" ["outs"]
       ; putStr $ show trainingData
       ; putStr "\n\n"
       ; target <- readFile $ head args
       ; putStr $ show $ bayes trainingData target
       ; putStr "\n\n"
       }
       
